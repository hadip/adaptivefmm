/* 
This is an adaptive FMM 3D code based on Black box chebyshev points approximation developed by Hadi Pouransari
   (contact: hadip@stanford.edu) 
*/

#include <iostream>
#include <fstream>
#include "node.h"	
#include "triple.h"	
#include "position.h"	
#include "routines.h"	
#include "chebyshev.h"
#include "kernel.h"
#include "precomputation.h"
#include "cpu_kernels.h"

int main ( int argc, char * const argv[] ) 
{
  clock_t begin = clock();
 
  if ( argc != 5 )
    {
      std::cerr << "Please supply a data file, followed by threshold, maximum depth of the tree, and number of chebyshev points in each direction. " 
		<< std::endl << "For example: ./fmm data.dat 64 5 4" << std::endl;
      exit(1);
    }
  std::ifstream input( argv[1] );
  int thresh = atoi( argv[2] );
  int lmax = atoi(argv[3]);
  int n = atoi(argv[4]);
  int N;
  input >> N;
  std::cout << " Number of points =" << N << std::endl;
  float4 *X;
  X=new float4[N];
  for ( int i(0); i<N; i++ )
    input >> X[i].x >> X[i].y >> X[i].z >> X[i].w;
  
  float3 a;
  a.x = -1;
  a.y = -1;
  a.z = -1;

  float3 b;
  b.x = 1;
  b.y = 1;
  b.z = 1;
  
  float *exact;
  
  clock_t end = clock();
  std::cout << "Elapsed time for data loading = " << diffclock( end, begin ) << " ms" << std::endl;
  
  chebyshev CHEB( n );
  
  CHEB.set_chebyshev_nodes();
  CHEB.set_chebyshev_polynomials();
  
  kernel KERN;	
  
  begin=clock();      
  precomputation PCOMP( &CHEB );
  end=clock();
  std::cout << "Elapsed time for preComputation = " << diffclock( end, begin ) << " ms" << std::endl;
  
  begin = clock();
  node T( thresh, lmax, N, X, a, b, 0, &CHEB, &PCOMP, &KERN );
  end = clock();
  std::cout << "Elapsed time for building tree = " << diffclock( end, begin ) << " ms" << std::endl;
  
  begin = clock();
  exact = CPU_Direct_solver( T.data(), N );			
  end = clock();
  std::cout << "Elapsed time for direct solver = " << diffclock( end, begin ) << " ms" << std::endl;
  
  begin = clock();
  make_list_C_V( &T );
  end = clock();
  std::cout << "Elapsed time for creating C and V lists " << diffclock( end, begin ) << " ms" << std::endl;
  
  begin = clock();
  make_list_U_X_W( &T );
  end = clock();
  std::cout << "Elapsed time for creating U and X and W lists = " << diffclock( end, begin ) << " ms" << std::endl;
  
  begin = clock();
  leaves_traversal( &T, CPU_P2M );
  end = clock();
  std::cout << "Elapsed time for P2M = " << diffclock( end, begin ) << " ms" << std::endl;
  
  begin = clock();
  postorder_traversal_BFS( &T, CPU_M2M );
  end = clock();
  std::cout << "Elapsed time for M2M = " << diffclock( end, begin ) << " ms" << std::endl;
  
  begin = clock();
  preorder_traversal_DFS( &T, CPU_M2L );
  end = clock();
  std::cout << "Elapsed time for M2L = " << diffclock( end, begin ) << " ms" << std::endl;
  
  begin = clock();
  preorder_traversal_DFS( &T, CPU_P2L );
  end = clock();
  std::cout << "Elapsed time for P2L = " << diffclock( end, begin ) << " ms" << std::endl;
  
  begin = clock();
  preorder_traversal_BFS( &T, CPU_L2L );
  end = clock();
  std::cout << "Elapsed time for L2L = " << diffclock( end, begin ) << " ms" << std::endl;
  
  begin = clock();
  leaves_traversal( &T, CPU_L2P );
  end = clock();
  std::cout << "Elapsed time for L2P = " << diffclock( end, begin ) << " ms" << std::endl;
  
  begin = clock();
  leaves_traversal( &T, CPU_P2P );
  end = clock();
  std::cout << "Elapsed time for P2P = " << diffclock( end, begin ) << " ms" << std::endl;
  
  begin = clock();
  leaves_traversal( &T, CPU_M2P );
  end = clock();
  std::cout << "Elapsed time for M2P = " << diffclock( end, begin ) << " ms" << std::endl;
  
  std::cout << " Averaged Rel. error = " << relative_error( &T, exact ) << 
    " for # of chebyshev nodes in each direction = " << n << std::endl <<
    "Maximum depth of the tree= " << T.max_depth() << std::endl;
  std::cout << std::endl;
  delete [] X;
  input.close();
}
