#include<iostream>
#include<fstream>
#include <stdio.h>
#include <stdlib.h>
#include<math.h>

using namespace std;
int main(int argc,char * const argv[])
{
  const float PI=3.14159265359;
  ofstream output(argv[2]);
  if (argc!=3)
    {
      cout<<"please enter number of points and output file address (e.g. 1000 data.dat)"<<endl;
      exit(1);
    }
  int N=atoi(argv[1]);
  float x,y,z,r,theta,phi;
  int Ntheta,Nphi;
  Ntheta=int(sqrt(N/2));
  Nphi=(N/2)/Ntheta;
  cout<<"Actual number of points= 2x"<<Ntheta<<"x"<<Nphi<<" = "<<Ntheta*Nphi*2<<endl;
  output<<Ntheta*Nphi*2<<endl;	
  for (int i(1);i<Ntheta+1;i++)
    for (int j(1);j<Nphi+1;j++)
      {
	theta=(i*PI)/float(Ntheta+1);
	phi=(j*2.*PI)/float(Nphi+1);
	x=0.5*sin(theta)*cos(phi);
	y=0.5*sin(theta)*sin(phi);
	z=0.5*cos(theta);
	output<<x<<" "<<y<<" "<<z<<" "<<1<<endl;
	output<<2*x<<" "<<2*y<<" "<<2*z<<" "<<1<<endl;
      }
  output.close();
}
