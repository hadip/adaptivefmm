#ifndef precomputation_h
#define precomputation_h

class chebyshev;

class precomputation
{
  float3 midpoint(float3,float3);
  chebyshev *cheb;
  int n;
 public:
  float ***matrix_M2M;
  float ***matrix_L2L;
  float ****matrix_M2L;
  precomputation(){}
  ~precomputation(){}
  precomputation(chebyshev*);
  void precompute();
};

# endif
