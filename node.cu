#include "node.h"
#include "routines.h"
#include "chebyshev.h"
#include "precomputation.h"
#include <vector>
#include <math.h>
#include <queue>
#include "position.h"
#include "triple.h"
node::node(int thresh,int max_l,int numb,float4 *dat,float3 A,float3 B,node* p,chebyshev* cb,precomputation* pc,kernel* kk)
{
  kern = kk;
  cheb = cb;
  pcomp = pc;
  n = cheb->n;
  M_ = new float[n*n*n];
  L_ = new float[n*n*n];
  isleaf_ = false;
  max_depth_ = 0;
  
  for (int i(0);i<8;i++)
    {
      children_[i]=NULL;
    }
 
  parent_ = p;
  
  if ( parent() == NULL ) 
    {
      level_ = 0;
    }
  else 
    {
      level( parent()->level() + 1 );
    }
  
  which_ = 0;
  threshold_ = thresh;
  lmax_ = max_l;
  num_ = numb;
  data_ = dat;
  coordinate.set_diag(A,B);
  coordinate.set_all();

  //M-data and L-data should be zero at first
  for (int i(0);i<n*n*n;i++)
    {
      M_[i] = 0;
      L_[i] = 0;
    }

  //creating lists (PreOrder)
  //A LIST:
  if (parent_!=NULL) 
    {
      for ( int i(0); i < parent_->list_A()->size(); i++ )
	{
	  list_A_.push_back( parent_->list_A()->begin()[i] );
	}
      list_A_.push_back( parent_ );
    }
  
  ///////////////////////////////creating children octants (if any!)///////////////////////////////
  if ( ( numb > threshold_ ) && ( level() < lmax_ ) )
    {
      divide();
    }
  else
    {
      isleaf_ = true;
      //for leaves, creat potentials (f) and make them zero initially
      f_ = new float[num_];
      for ( int i(0); i < num_; i++ )
	{
	  f_[i] = 0;
	}
    }

   //creating lists (PostOrder)
   //L LIST:
   if ( isleaf_ ) 
     {
       list_L_.push_back( this );
     }
   else
     {
       for ( int i(0); i < 8; i++ )
	 {
	   if ( children_[i] != NULL )
	     {
	       for ( int j(0); j < children_[i]->list_L()->size(); j++ )
		 {
		   list_L_.push_back( children_[i]->list_L()->begin()[j] );
		 }
	     }
	 }
     }
   
   //D LIST
   if ( !isleaf_ )
     {
       for ( int i(0); i < 8; i++ )
	 {
	   if ( children_[i] != NULL )
	     {
	       list_D_.push_back( children_[i] );
	       for ( int j(0); j < children_[i]->list_D()->size(); j++ )
		 {
		   list_D_.push_back( children_[i]->list_D()->begin()[j] );
		 }
	     }
	 }
     }

   //comput max_depth
   if ( !isleaf_ )
     {
       for ( int i(0); i < 8; i++ )
	 {
	   if ( children_[i] != NULL )
	     {
	       if ( children_[i]->max_depth()+1 > max_depth_ )
		 {
		   max_depth_ = children_[i]->max_depth() + 1;
		 }
	     }
	 }
     }
}

node::~node()
{  
  for ( int i(0); i < 8; i++ )
    {
      if ( children_[i] != NULL)  
	{
	  delete children_[i];
	}
    }
  if ( isleaf_ ) 
    {
      delete [] f_;
    }
  delete [] M_;
  delete [] L_;
}

int node::sortbyx( int start, int finish ) //for now, I use extra memory to do this !
{
  //length of interval of interest
  int num = finish - start;
  
  //calculate size of each part
  int U(0);
  for ( int i(start); i < finish; i++ )
    {
      if ( data_[i].x <=  coordinate.center().x ) 
	{
	  U++;
	}
    }
  
  //allocate new memory
  float4 *temp;
  temp = new float4[num];

  //pseudo sort
  int D(0);
  for ( int i(start); i < finish; i++ )
    {
      if ( data_[i].x <= coordinate.center().x )
	{
	  temp[D++] = data_[i];
	}
      else 
	{
	  temp[U++] = data_[i];
	}
    }
  
  //copy back to main array
  for ( int i(0); i < num; i++ )
    {
      data_[i+start] = temp[i];
    }

  //free memory
  delete [] temp;
  return ( D + start );
}

int node::sortbyy( int start, int finish ) 
{
  int num = finish - start;
  int U(0);
  for ( int i(start); i < finish; i++ )
    {
      if ( data_[i].y <= coordinate.center().y)
	{
	  U++;
	}
    }
  float4 *temp;
  temp = new float4[num];
  int D(0);
  for ( int i(start); i < finish; i++ )
    {
      if ( data_[i].y <= coordinate.center().y ) 
	{
	  temp[D++] = data_[i];
	}
      else 
	{
	  temp[U++] = data_[i];
	}
    }
  for ( int i(0); i < num; i++)
    {
      data_[i+start] = temp[i];
    }
  delete [] temp;
  return ( D + start );
}

int node::sortbyz( int start, int finish )
{
  int num = finish - start;
  int U(0);
  for ( int i(start); i < finish; i++ )
    {
      if ( data_[i].z <= coordinate.center().z )
	{
	  U++;
	}
    }
  float4 *temp;
  temp = new float4[num];
  int D(0);
  for ( int i(start); i < finish; i++ )
    {
      if ( data_[i].z <= coordinate.center().z )
	{
	  temp[D++] = data_[i];
	}
      else 
	{
	  temp[U++] = data_[i];
	}
    }
  for ( int i(0); i < num; i++ )
    {
      data_[i+start] = temp[i];
    }
  delete [] temp;
  return ( D+start );
}

void node::divide()
{
  int x1, y1, y2, z1, z2, z3, z4;
  position temp1( coordinate.corners()[0], coordinate.center() );
  position temp2( coordinate.center(), coordinate.corners()[7] );
  temp1.set_all();
  temp2.set_all();
  x1 = sortbyx( 0, num_ );
  y1 = sortbyy( 0, x1 );  
  y2 = sortbyy( x1, num_ );
  z1 = sortbyz( 0, y1) ;
  z2 = sortbyz( y1, x1 ); 
  z3 = sortbyz( x1, y2 );
  z4 = sortbyz( y2, num_ );

  int C[9] = { 0, z1, y1, z2, x1, z3, y2, z4, num_ };
  
  for ( int i(0); i < 8; i++ )
    {
      if ( C[i+1] > C[i] )
	{
	  children_[i] = new node( threshold_, lmax_, C[i+1] - C[i], &data_[C[i]], temp1.corners()[i], temp2.corners()[i], this, cheb, pcomp, kern );
	  children()[i]->which(i);
	  children()[i]->which_binary(i);
	}
    }
}

bool node::is_inside(float3 A)
{
  bool X,Y;
  X = ( coordinate.corners()[0].x - A.x<=epsilon ) &&	\
    ( coordinate.corners()[0].y - A.y <= epsilon ) &&	\
    ( coordinate.corners()[0].z - A.z <= epsilon);	
  
  Y = ( coordinate.corners()[7].x - A.x >= -epsilon ) &&
    ( coordinate.corners()[7].y - A.y >= -epsilon ) &&
    ( coordinate.corners()[7].z - A.z >= -epsilon );
  
  return ( X && Y );
}

bool node::is_adjacent( node *M )
{
  bool R = true;
  R = R && ( abs( M->coordinate.center().x - coordinate.center().x ) <= 
	     epsilon + 0.5 * ( M->coordinate.corners()[7].x - M->coordinate.corners()[0].x + coordinate.corners()[7].x - coordinate.corners()[0].x ) );
  R = R && ( abs( M->coordinate.center().y - coordinate.center().y ) <= 
	     epsilon + 0.5 * ( M->coordinate.corners()[7].y - M->coordinate.corners()[0].y + coordinate.corners()[7].y - coordinate.corners()[0].y ) );
  R = R && ( abs( M->coordinate.center().z - coordinate.center().z ) <= 
	     epsilon + 0.5 * ( M->coordinate.corners()[7].z - M->coordinate.corners()[0].z + coordinate.corners()[7].z - coordinate.corners()[0].z ) );
  return R;
}


void node::make_list_C_V()
{
  node *A;
  triple temp;
  if ( parent_ != NULL )
    {
      //first add siblings
      for ( int i(0); i < 8; i++ )
	{
	  A = parent_->children()[i];
	  if ( ( A != NULL ) && ( i != which_ ) )
	    {
	      list_C_.push_back(A);
	      temp = A->which_binary() - which_binary_;
	      temp.pointer = A;
	      colleagues_[temp.t2i()] = temp;
	    }
	}
      //And then add adjacent children of my parents' colleagues
      for ( int i(0); i < 27; i++ )
	{
	  if ( parent_->colleagues()[i].pointer != NULL )
	    {
	      for ( int j(0); j < 8; j++ )
		{
		  A = parent_->colleagues()[i].pointer->children()[j];
		if ( A != NULL )
		  {
		    temp = parent_->colleagues()[i] * 2 + A->which_binary() - which_binary_;
		    temp.pointer = A;
		    if ( temp.norm() == 1 ) 
		      {
			colleagues_[temp.t2i()] = temp;
			list_C_.push_back(A);
		      }
		    else 
		      {
			interactants_[ ( temp + which_binary_ ).h2i() ] = temp;
			list_V_.push_back(A);
		      }
		  }
		}
	    }
	}
    }
}

void node::make_list_U_X_W()
{
  //first add my leaf collegues
  node *A;
  node *B;
  for ( int i(0); i < list_C_.size(); i++ )
    {
      A = list_C_.begin()[i];
      if ( A->isleaf() ) 
	{
	  list_U_.push_back(A);
	}
    }

  //then distribute my U_list to my children if I am not a leaf !
  if ( !isleaf_ )
    {
      while ( list_U_.size() > 0 )
	{
	  A = list_U_.back();
	  for ( int i(0); i < 8; i++ )
	    {
	      B = children_[i];
	      if ( children_[i] != NULL )
		{
		  if (B->is_adjacent(A)) B->list_U()->push_back(A);
		  else 
		    {
		      A->list_W()->push_back(B);
		      B->list_X()->push_back(A);
		    }
		}
	    }
	  list_U_.pop_back();
	}
    }
  
  for ( int i(0); i < 8; i++ )
    {
      if ( children_[i] != NULL ) 
	{
	  children_[i]->make_list_U_X_W();
	}
    }
}

//RELATED ALGORITHMS
void make_list_C_V( node *T )
{
  std::queue<node *> stack;
  stack.push(T);
  while ( stack.size() > 0 )
    {
      T = stack.front();
      stack.pop();
      T->make_list_C_V();
      for ( int i(0); i < 8; i++ )
	{
	  if ( T->children()[i] != NULL ) 
	    {
	      stack.push( T->children()[i] );
	    }
	}
    }
}


void make_list_U_X_W( node *T )
{
  //first make sure each leaf has list of all equal/bigger adjacent leaves
  T->make_list_U_X_W();
  
  //now going through all leaves add itself to U-list of all leaves in its U-list if I am strictly smaller !
  node *A;
  node *B;
  for ( int i(0); i < T->list_L()->size(); i++ )
    {
      A = T->list_L()->begin()[i];
      for ( int j(0); j < A->list_U()->size(); j++ )
	{
	  B = A->list_U()->begin()[j];
	  if ( B->level() < A->level() ) 
	    {
	      B->list_U()->push_back(A);
	    }
	}
    }
  //Now add each leaf to its own U-list =>the last member in U-list is always the leaf itself
  for ( int i(0); i < T->list_L()->size(); i++ )
    {
      A = T->list_L()->begin()[i];
      A->list_U()->push_back(A);
    }
}
