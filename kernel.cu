#include "kernel.h"

float kernel::k( float3 X, float3 Y )
{
  float distance = sqrt( (X.x - Y.x) * (X.x - Y.x ) + (X.y - Y.y) * (X.y - Y.y) + (X.z - Y.z) * (X.z-Y.z) );
  return 1. / distance;
}


float kernel::k( float4 X, float3 Y )
{
  float distance = sqrt( (X.x - Y.x) * (X.x - Y.x ) + (X.y - Y.y) * (X.y - Y.y) + (X.z - Y.z) * (X.z-Y.z) );
  return 1. / distance;
}

float kernel::k( float3 X, float4 Y )
{
  float distance = sqrt( (X.x - Y.x) * (X.x - Y.x ) + (X.y - Y.y) * (X.y - Y.y) + (X.z - Y.z) * (X.z-Y.z) );
  return 1. / distance;
}


float kernel::k( float4 X, float4 Y )
{
  float distance = sqrt( (X.x - Y.x) * (X.x - Y.x ) + (X.y - Y.y) * (X.y - Y.y) + (X.z - Y.z) * (X.z-Y.z) );
  return 1. / distance;
}
