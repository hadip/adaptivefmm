#ifndef cpu_kernels_h
#define cpu_kernels_h

/*
Implementation of all FMM operations: P2P P2M M2L M2M L2L M2P P2L L2P
 */
#include "chebyshev.h"
#include "node.h"
#include "kernel.h"
#include <fstream>
#include <math.h>
#include <queue>
#include "precomputation.h"

float* CPU_Direct_solver( float4 *A, int N )
{
  kernel kern;
  float *ans = new float[N];
  for (int i(0);i<N;i++)
    ans[i] = 0;
  for ( int i(0); i < N; i++ )
    {
      for ( int j(i+1); j < N; j++ )
	{
	  ans[i] += A[j].w * kern.k( A[i], A[j] );
	  ans[j] += A[i].w * kern.k( A[j], A[i] );
	}
    }
  std::ofstream output("direct_calculation.dat");
  for ( int i(0); i < N; i++ )
    {
      output << A[i].x << " " << A[i].y << " " << A[i].z << " " << ans[i] << std::endl;
    }
  output.close();
  return ans;
}

void show( node *T )
{
  for ( int i(0); i < T->num(); i++ )
    {
      std::cout << i << ": " << "   " << T->data()[i].x << 
	" " << T->data()[i].y << " " << T->data()[i].z << " ";
      if ( T->isleaf() )
	{
	  std::cout << T->f()[i] << std::endl;
	}
      else 
	{
	  std::cout << std::endl;
	}
    }
}

void CPU_P2M( node* T )
{
  chebyshev* cheb = T->cheb;
  int n = cheb->n;
  float4 J;
  for ( int j(0); j < T->num(); j++ )
    {
      J = cheb->scale( T->data()[j], T->coordinate.center(), (float) ( 1 << ( T->level() ) ) );
      for ( int i(0); i < n*n*n; i++ )
	{
	  T->M()[i]+=T->data()[j].w*cheb->R(n,J,cheb->nodes[i]); 
	}
    }
}


void CPU_M2M( node* T )
{  
  chebyshev* cheb = T->cheb;
  precomputation* pcomp = T->pcomp;
  int n = cheb->n;
  if ( T->isleaf() ) 
    {
      return;  
    }
  for ( int l(0); l < n*n*n; l++ )
    {
      for ( int i(0); i < 8; i++ )
	{
	  if ( T->children()[i] != NULL )
	    {
	      for ( int m(0); m < n*n*n; m++ )
		{
		  //using PreComputation
		  T->M()[l] += T->children()[i]->M()[m] * pcomp->matrix_M2M[i][l][m];
		}
	    }
	}
    }
}

void CPU_M2L( node *T )
{
  chebyshev* cheb = T->cheb;
  precomputation* pcomp = T->pcomp;
  int n = cheb->n;
  node *J;
  for ( int l(0); l < n*n*n; l++ )
    {
      for ( int j(0); j < 216; j++ )
	{
	  if ( T->interactants()[j].pointer != NULL )
	    {
	      J = T->interactants()[j].pointer;
	      for ( int m(0); m < n*n*n; m++ )
		{
		  //using precomputation and scalable kernel.
		  T->L()[l] += J->M()[m] * pcomp->matrix_M2L[T->which()][j][m][l] * ( 1 << (-T->kern->lambda) * T->level() );
		}
	    }
	}
    }
}

void CPU_P2L( node *T )
{
  chebyshev* cheb = T->cheb;
  int n = cheb->n;
  node *J;
  float3 Ls;
  for ( int l(0); l < n*n*n; l++ )
    {
      Ls = cheb->scale2( cheb->nodes[l], T->coordinate.center(), 1 << (T->level()) );
      for ( int j(0); j < T->list_X()->size(); j++ )
	{
	  J = T->list_X()->begin()[j];
	  for ( int i(0); i < J->num(); i++ )
	    {
	      T->L()[l] += J->data()[i].w * T->kern->k( Ls, J->data()[i] );
	    }
	}
    }
}

void CPU_L2L( node *T )
{
  chebyshev* cheb = T->cheb;
  precomputation* pcomp = T->pcomp;
  int n = cheb->n;
  if ( T->parent() == NULL ) 
    {
      return;
    }
  node *P = T->parent();
  for ( int l(0); l < n*n*n; l++ )
    {
      for ( int ll(0); ll < n*n*n; ll++ )
	{
	  //using PreComputation
	  T->L()[l] += P->L()[ll] * pcomp->matrix_L2L[T->which()][ll][l];
	}
    }
}

void CPU_L2P( node *T )
{
  chebyshev* cheb = T->cheb;
  int n = cheb->n;
  float4 I;
  for ( int i(0); i < T->num(); i++ )
    {
      I = cheb->scale( T->data()[i], T->coordinate.center(), 1 << (T->level()) );
      for ( int l(0); l < n*n*n; l++ )
	T->f()[i] += T->L()[l] * cheb->R( n, I, cheb->nodes[l] );
    }
}

void CPU_P2P( node *T )
{
  node *J;
  for ( int i(0); i < T->num(); i++ )
    {
      for ( int j(0); j < T->list_U()->size() - 1; j++ ) //going through all adjacent cell (except itself)
	{
	  J = T->list_U()->begin()[j];
	  for ( int i2(0); i2 < J->num(); i2++ )
	    {
	      T->f()[i] += J->data()[i2].w * T->kern->k( T->data()[i], J->data()[i2] );
	    }
	}
      for ( int i2(i+1); i2 < T->num(); i2++ ) //interaction between particles within Box T
	{
	  T->f()[i] += T->data()[i2].w * T->kern->k( T->data()[i], T->data()[i2] );
	  T->f()[i2] += T->data()[i].w * T->kern->k( T->data()[i2], T->data()[i] );
	}
    }
}

void CPU_M2P( node *T )
{
  chebyshev* cheb = T->cheb;
  int n = cheb->n;
  node *J;
  float3 Js;
  for ( int j(0); j<T->list_W()->size(); j++ )
    {
      J = T->list_W()->begin()[j];
      for ( int m(0); m < n*n*n; m++ )
	{
	  Js = cheb->scale2( cheb->nodes[m], J->coordinate.center(), 1 << (J->level()) );
	  for ( int i(0); i < T->num(); i++ )
	    {
	      T->f()[i] += J->M()[m] * T->kern->k( T->data()[i], Js );
	    }
	}
    }
}


//run function f on nodes of a tree with given root
void postorder_traversal_BFS( node* root, void (&f)(node*) )
{
  int counter(0);
  std::vector<node *> stack;
  stack.push_back(root);
  while ( stack.size() > counter )
    {
      root = stack[counter++];
      for ( int i(0); i < 8; i++ )
	{
	  if ( root->children()[i] != NULL )
	    {
	      stack.push_back( root->children()[i] );
	    }
	}
    }
  while ( stack.size() > 0 )
    {
      f( stack.back() );
      stack.pop_back();
    }
}


void postorder_traversal_DFS( node* root, void (&f)(node*) )
{
  for ( int i(0); i < 8; i++ )
    {
      if ( root->children()[i] != NULL)
	{
	  postorder_traversal_DFS( root->children()[i], f );
	}
    }
  f( root );
}


void preorder_traversal_BFS( node* root, void (&f)(node*) )
{
  std::queue<node*> stack;
  stack.push(root);
  while( stack.size() > 0 )
    {
      root = stack.front();
      stack.pop();
      f( root );
      for ( int i(0); i < 8; i++ )
	{	  
	  if ( root->children()[i] != NULL )
	    {
	      stack.push( root->children()[i] );
	    }
	}
    }
}

void preorder_traversal_DFS( node* root, void (&f)(node*) )
{
  f( root );
  for ( int i(0); i < 8; i++ )
    {
      if ( root->children()[i] != NULL ) 
	{
	  preorder_traversal_DFS( root->children()[i], f );
	}
    }
}

//run function f on leaves of the tree with given root
void leaves_traversal( node* root, void (&f)(node*) )
{
  for ( int i(0); i < root->list_L()->size(); i++ )
    {
      f( root->list_L()->begin()[i] );
    }
}


float relative_error( node *T, float *exact )
{
  std::ofstream out( "FMM_calculation.dat" );
  int c(0);
  float ans;
  node *A;
  for ( int i(0); i < T->list_L()->size(); i++ )
    {
      A = T->list_L()->begin()[i];
      for ( int j(0); j < A->num(); j++ )
	{
	  ans += abs( A->f()[j] - exact[c] ) / exact[c];
	  out << A->data()[j].x << " " << A->data()[j].y << " " << A->data()[j].z << " " << A->f()[j] << " " << exact[c] << std::endl;
	  c++;
	}
    }
  out.close();
  return ( ans / T->num() );
}

#endif
