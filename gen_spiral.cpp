#include<iostream>
#include<fstream>
#include <stdio.h>
#include <stdlib.h>
#include<math.h>

using namespace std;
int main(int argc,char * const argv[])
{
  ofstream output(argv[2]);
  if (argc!=3)
    {
      cout<<"please enter number of points and output file address (e.g. 1000 data.dat)"<<endl;
      exit(1);
    }
  int N=atoi(argv[1]);
  output<<N<<endl;
  float theta=0;
  float a=0;
  float b=0.03;
  float c=2;
  float d=0;
  float t=1/(b*N);
  float r,x,y,z;
  for (int i(0);i<N;i++)
    {
      r=a+b*theta;
      x=r*cos(theta)+d;
      y=r*sin(theta)+d;
      z=c*r-1;
      output<<x<<" "<<y<<" "<<z<<" "<<1<<endl;
      theta+=t;
    }
  output.close();
}
